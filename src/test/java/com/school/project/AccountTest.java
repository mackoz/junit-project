package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive() {
        // given
        Account account = new Account();

        //then
        assertFalse( account.isActive(),
                "Check if new account is not active" );

        assertThat( account.isActive(), equalTo( false ) );
        assertThat( account.isActive(), is( false ) );


    }


    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation() {
        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue( account.isActive(),
                "Check if new account is not active" );

    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull() {

        //1
//        //given
//        Account account = new Account();
//        //when
//        Address address = account.getDefaultDeliverAddress();
//        //then
//        assertNull(address);

        // 2
        //given
        Account account1 = new Account();

        //then
        assertNull( account1.getDefaultDeliverAddress() );

        assertThat( account1.getDefaultDeliverAddress(), nullValue() );


    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSetInAccount() {

        // 2
        //given
        Account account1 = new Account();

        //when
        account1.setDefaultDeliverAddress( new Address( "Szkolna", "100a" ) );

        //then
        assertNotNull( account1.getDefaultDeliverAddress() );

        assertThat( account1.getDefaultDeliverAddress(), notNullValue() );

    }


    @Test
    void chceckIsEmailIsNotNull() {
//        given
        Account account1 = new Account();

//        when
        account1.setEmailAdress( "james@gmail.com" );

//        then
        assertThat( account1.getEmailAdress(), notNullValue() );

    }

    @Test
    public void chceckIsEmailIsCorrect() {
//        given

    Account account1 = new Account();

//        then
          Assertions.assertThrows(IllegalArgumentException .class,()->
    {
        account1.setEmailAdress( "jamesemail.com" );
    });
}

    @Test
    public void chceckIsEmailIsCorrecLenght() {
//        given

        Account account1 = new Account();

//        then

        account1.setEmailAdress( "aaaaaaaaaaaaa" );
        account1.setEmailAdress( "10@gmail" );

        Assertions.assertThrows( IllegalArgumentException.class, () -> {
            account1.setEmailAdress( "gm@wp.pl" );
        } );
    }


@ParameterizedTest
@ValueSource(strings = {"daniel@gmail.com", "james@gmail.com", "steve@gmail.com"})
    void correctEmailAdressShouldNotBeEmptyAfterSettingUp (String strFromValueSource){

        Account account = new Account(  );
        account.setEmailAdress( strFromValueSource );
        assertThat(account.getEmailAdress(),  notNullValue());

}


}