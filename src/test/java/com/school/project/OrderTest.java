package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void addMealToOrder() {
    }

    @Test
    void removeMealFromOrder() {
    }

    @Test
    void getMeals() {
    }


    @Test
    void testAssertArrayEquals() {
//        given
        int[] ints1 = {1, 2, 3};
        int[] ints2 = {1, 2, 3};
//        then
        assertArrayEquals( ints1, ints2 );
    }

    @Test
    void mealListShouldBeEmptyAfterCreationOfOrder() {
        Order order = new Order();

        assertThat( order.getMeals(), empty() );
        assertThat( order.getMeals().size(), equalTo( 0 ) );
        assertThat( order.getMeals(), hasSize( 0 ) );
        assertThat( order.getMeals(), emptyCollectionOf( Meal.class ) );
    }


    @Test
    void addingMealToOrderShouldIncreaseOrderSize() {
//        given
        Meal meal = new Meal( 15, "Kebab" );
        Order order = new Order();

        order.addMealToOrder( meal );
        assertThat( order.getMeals(), hasSize( 1 ) );
    }

    @Test
    void addingMealToOrderShouldIncreaseOrderListSize() {
        Meal meal1 = new Meal( 10, "Burger" );
        Meal meal2 = new Meal( 12, "Pizza" );
        Order order = new Order();
        order.addMealToOrder( meal1 );
        assertThat( order.getMeals(), contains( meal1 ) );
        assertThat( order.getMeals(), not( contains( meal2 ) ) ); // test will fail
        assertThat( order.getMeals(), hasItem( meal2 ) );
        assertThat( order.getMeals().get( 0 ).getPrice(), equalTo( 10 ) );

    }

    @Test
    void removingMealFromOrderShouldDecreaseOrderSize() {
        Meal meal1 = new Meal( 10, "Burger" );

        Order order = new Order();
        order.addMealToOrder( meal1 );
        order.removeMealFromOrder( meal1 );
        assertThat( order.getMeals(), not( contains( meal1 ) ) );
        assertThat( order.getMeals(), empty() );
        assertThat( order.getMeals(), hasSize( 0 ) );
    }

    @Test
    void mealsShouldBeInCorrectOrderAfterAddingThemToOrder() {
        Meal meal1 = new Meal( 10, "Burger" );
        Meal meal2 = new Meal( 12, "Pizza" );

        Order order = new Order();

        order.addMealToOrder( meal1 );
        order.addMealToOrder( meal2 );

//sprawdzanie kolejnosci
//        assertThat( order.getMeals(), contains( meal1, meal2 ) ); //test fail wrong ordest


        assertThat( order.getMeals(), contains( meal1, meal2 ) );
        assertThat( order.getMeals(), hasItem( meal1 ) );
        assertThat( order.getMeals(), hasItems( meal1, meal2 ) );

        assertThat( order.getMeals(), containsInAnyOrder( meal1, meal2 ) );
    }

    @Test
    void testIfTwoOrdersAreTheSame() {
        Meal meal = new Meal( 10, "Burger" );
        Meal meal2 = new Meal( 12, "Pizza" );
        Meal meal3 = new Meal( 22, "Pierogi" );

        List<Meal> mealList1 = Arrays.asList( meal, meal2 );
        List<Meal> mealList2 = Arrays.asList( meal, meal2 );
        List<Meal> mealList3 = Arrays.asList( meal2, meal );
        List<Meal> mealList4 = Arrays.asList( meal2, meal, meal3 );

        Order order1 = new Order();
        Order order2 = new Order();

        order1.addMealToOrder( mealList1 );
//        order2.addMealToOrder(mealList2);
//        order2.addMealToOrder(mealList3); homework make this scenario pass test

        assertThat( order1.getMeals(), is( order2.getMeals() ) );


    }
}