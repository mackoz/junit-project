package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void userIsNull() {
//given
        User user = new User();

        //then
        assertNull( user.getName() );


    }
    @Test
    public void userUnder18(){
        User user = new User();
        user.setAge(  15);

        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            user.getAge();
        });
    }





}