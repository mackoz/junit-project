package com.school.project;

public class User {
    private String name;
    private int age;





    private void userUnder18(int age) {
        if (age < 18) {
            this.age = age;
        } else {
             {
                throw new IllegalArgumentException( "Age Under 18!" );
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
