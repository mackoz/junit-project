package com.school.project;

public class Account {

    private boolean active;
    private Address defaultDeliverAddress;
    private String emailAdress;

    public Account() {
    }

    public Account(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        this.active = true;
    }

    public Address getDefaultDeliverAddress() {
        return defaultDeliverAddress;
    }

    public void setDefaultDeliverAddress(Address address) {
        this.defaultDeliverAddress = address;
    }



    public static void main(String[] args) {

        Account account = new Account();
        System.out.println(account.isActive());
        //

        Account account1 = new Account();
        account1.activate();
        System.out.println(account1.isActive());

        //
        Account account2 = new Account();

        System.out.println(account2.getDefaultDeliverAddress()); // null

    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
//        check if contains
        if (emailAdress.contains( "@" ) && emailAdress.length() > 10 ){
        this.emailAdress = emailAdress;
        }
         else {
            throw new IllegalArgumentException("wrong adres or too short");
        }
    }
}
